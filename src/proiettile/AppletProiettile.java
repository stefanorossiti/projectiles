package proiettile;

/************************************************************
* Autore: Rossi Stefano
* Nome: AppletProiettile
* Descrizione: simula la fisica del lancio di un proiettile 
* tramite i parametri inseriti
************************************************************/
import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class AppletProiettile extends Applet implements Runnable, MouseMotionListener, MouseListener, ListenerProiettile{
    Thread thAgg = new Thread(this);
    Image imgBuff; //dichiarazione oggetti per doppio buffering
    Graphics grafBuff;
    Point puntaV = new Point(); // punta del vettore velocita
    Point piedeV = new Point(); // piede del vettore velocita
    Point originAx = new Point(); //origine del diagramma cartesiano 
    double intV;
    double angV;
    int heightP; //altezza e larghezza del proiettile
    int widthP;
    Proiettile[]  p = new Proiettile[62]; //max proiettili
    int pCNT; //conteggio dei proiettii
    double lungCampo, altCampo; //altezza e lunghezza campo in pixel
    Image imgCann;
    Image imgBgCamp;

    public void init(){
        this.setSize(1200, 600); 
        
        imgBuff = createImage(1200, 600);
        grafBuff = imgBuff.getGraphics();
        
        imgCann = getImage(getCodeBase(), "img/cann.png");
        imgBgCamp = getImage(getCodeBase(), "img/bgCampo.png");
        //System.out.println(getCodeBase());
        
        this.addMouseMotionListener(this);
        this.addMouseListener(this);
        
        originAx.setLocation(50, 550);
        
        heightP = 13;
        widthP = 13;
       
        resetV();
        
        lungCampo = 1100;
        altCampo = 500;
        
        pCNT = 0;
        
        //solo se il repaint e gestito dall applet
        //thAgg.start(); 
    }

    @Override
    public void update(Graphics g){
        paint(g);
    }

    @Override
    public void paint(Graphics g){
            //cancello tutto in grafbuff
            grafBuff.clearRect(0, 0,imgBuff.getWidth(this), imgBuff.getHeight(this));
            
            //disegno un quadrato per i bordi
            grafBuff.drawRect((int)originAx.getX(), (int)(originAx.getY() - altCampo), (int)lungCampo, (int) altCampo);
            
            //disegno i proiettili rimanenti in basso
            for(int i = 1; i < p.length - pCNT; i++){
                grafBuff.fillOval((int)originAx.getX() + 5*(i-1) + widthP*(i-1) + 5,(int)originAx.getY() + 8,  widthP, heightP);
            }
            
            //disegno l immagine del cannone e dello sfondo del campo
            grafBuff.drawImage(imgCann, (int)originAx.getX() - imgCann.getWidth(this), (int)originAx.getY() - imgCann.getHeight(this)/2, this);
            grafBuff.drawImage(imgBgCamp, (int)originAx.getX(), (int)(originAx.getY() - altCampo), this);
            
            //disegno i proiettile sparati nel campo
            for(int i = 0; i < pCNT && i < p.length && p[i] != null; i++){
                grafBuff.fillOval((int)originAx.getX() + p[i].getX() - 5, (int)originAx.getY() - p[i].getY() - heightP, widthP, heightP);
            }
            //disegno il vettore V con i suoi parametri se possibile
            if(piedeV.getX() != -1){
                grafBuff.drawLine((int)piedeV.getX(), (int)piedeV.getY(), (int)puntaV.getX(), (int)puntaV.getY());
                grafBuff.drawString("|V| =  " + String.valueOf(Math.rint(intV*100)/100) + "px/sec", (int)(puntaV.getX()/2 - piedeV.getX()/2 + piedeV.getX()), (int)(puntaV.getY()/2 - piedeV.getY()/2 + piedeV.getY() + 10));
                grafBuff.drawString("angV = " + String.valueOf(Math.rint(Math.toDegrees(angV)*100)/100) + "°", (int)(puntaV.getX()/2 - piedeV.getX()/2 + piedeV.getX()), (int)(puntaV.getY()/2 - piedeV.getY()/2 + piedeV.getY() + 25));
            }
            
            g.drawImage(imgBuff, 0, 0, this);
    }

    @Override
    public void run(){
            while(this.thAgg.isAlive()){
                    repaint();
                    System.out.println("repaint");
                    try{
                        Thread.sleep(1000);
                    }catch(Exception e){
                        System.out.println("thread exception");
                    }
            }
    }
    
    //azzero i valori di V
    private void resetV(){
        piedeV.setLocation(-1,-1);
        puntaV.setLocation(-1,-1);
        intV = 0;
        angV = 0;
    }
    //in questo metodo calcolo il vettore n coordinate polari di V
    private void computeV(){
        double vx = puntaV.getX() - piedeV.getX(); //componente orizzontale x di V
        double vy = piedeV.getY() - puntaV.getY(); //componente orizzontale y di V x0 e in alto
        
        intV = Math.sqrt(vx*vx + vy*vy);
        angV = Math.atan(vy/vx); //radianti
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1){
            puntaV.setLocation(e.getPoint());
        }
    }
        
    @Override
    public void mouseDragged(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1){
            piedeV.setLocation(e.getPoint());
            computeV();
            repaint();
        }
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
        //istanzio un nuovo proiettile e incremento la conta dei proiettili se possibile
        if(pCNT < p.length){
            p[pCNT] = new Proiettile(0, 0, widthP, heightP, lungCampo, altCampo, intV, angV, 9.81, 40, 5, this);   
            pCNT++;
            
            //se ho sparato tutti i proiettili cambio l immagine del cannone con quella vuota
            if(pCNT == p.length){
                imgCann = getImage(getCodeBase(), "img/cannEmpty.png");
            }
        }
        
        //reimposto le coordinate dei punti del vettore fuori dal contesto grafico
        resetV();
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void hasMoved(Point newPos) {
        //System.out.println("mosso");
        repaint();
    }
}