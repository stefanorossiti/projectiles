package proiettile;
import java.awt.Point;

public class Proiettile implements Runnable{
    Point position; //posizione del proiettile
    int widthP, heightP;
    double maxX, maxY; //quanto in alto e in lungo puo andare il priettile prima di rimbalzare
    private ListenerProiettile lp;
    Thread t;
    double intV; //la velocita del priettile in pixel al secondo
    double angV; //angolo della velocita del proiettile
    double g; //pixel al secondo
    double atrTerreno; //attrito dinamico terreno deceleraz pixel/sec
    double enrgAssPerc; //percentuale energia cinetica ssorbita dopo l impatto con una parete
    

    public Proiettile(double x, double y, int widthP, int heightP, double maxX, double maxY, double vProj, double angvProj, double gravity, double enrgAssPerc, double atrTerreno, ListenerProiettile lp){
            this.lp = lp;
            position = new Point((int)x, (int)y);
            this.maxX = maxX;
            this.maxY = maxY;
            this.g = gravity;
            this.widthP = widthP;
            this.heightP = heightP;
            
            //controllo se la percentuale di energia assorbita e compresa tra 0 e 100 se no la imposto correttamente
            if(enrgAssPerc >= 0 && enrgAssPerc <= 100){
                this.enrgAssPerc = enrgAssPerc/100; //percentuale in decimale
            }else if(enrgAssPerc < 0){
                this.enrgAssPerc = 0;
            }else{
                this.enrgAssPerc = 100;
            }
            
            this.atrTerreno = atrTerreno;
            
            t = new Thread(this);
            this.intV = vProj/5;
            this.angV = angvProj;
            t.start();
    }

    private void newPos(){
        //se la veloci del proiettie non e 0 allora calcolo la nuova posizione
        if(intV != 0){
            double time = 0.1; //ricalcolo la posizione dopo 0.1 dec di secondo 
             
            //componente x della velocita
            double vx = this.intV * Math.cos(angV);
            //componente y della velocita
            double vy = this.intV * Math.sin(angV);
            
            //calcolo la nuova posizione
            // MRU Sf = si + v*t
            double nextX = this.position.getX() + vx * time;
            // MRUA 
            double nextY = this.position.getY() + vy * time - 0.5 * g * time * time;
            //System.out.println("vx: " + vx + " vy: " + vy);
            
            //salvo la nuova positione
            this.position.setLocation(nextX, nextY);
            
            //calcolo la nuova velocita
            vy = vy - g * time; //calcolo di quanto g fa diminuore la velocita verticale
            
            //se la pallina tocca l terra rimbalza
            //se il modulo della sua velocita e minore della gravita si ferma in vy
            //altrimenti controlla se la pallina tocca il cielo
            if(this.position.getY() < 0 || this.position.getY() + heightP > maxY){
                vy = invertVY(vy);
                
                //se l intensita della velocita y e min di quanto puo essere decell da g in questo lasso di tempo viene settata a 0 esatto
                if(Math.abs(vx) < g * time*time){
                    vy = 0;
                }
            }
            
            //se la pallina tocca il muro destro rimbalza
            //se il modulo della sua velocita e minore della gravita si ferma in vy
            //altrimenti se tocca quello di sx
            if(this.position.getX() + widthP > maxX || this.position.getX() < 0){
                vx = invertVX(vx); 
            }
            
            //faccio in modo di rallentare leggermente vx se necessario
            vx = slowVX(vx, vy, time);
            //System.out.println(vx);
            
            //rivalvolo angolo e intensita con la nuova vy
            this.intV = Math.sqrt(vy*vy + vx*vx);
            this.angV = Math.atan(vy/vx);
            
            //per imprecisione della trigonometria l angolo perde il segno con la tangente quindi bisogna calcolarlo manualmente
            //questo vuol dire invertire il segno o diminuire/aumentare l angolo di PI radianti
            if(vx < 0){
                this.angV -= Math.PI;
            }
            
            if(vx==0 && vy == 0){
                this.intV = 0;
                this.angV = 0;
            } 
            
            //System.out.println(vx);
        }
    }

    public int getX(){
        return (int)this.position.getX();
    }

    public int getY(){
        return (int)this.position.getY();
    }
    
    private double invertVX(double vx){
        //se l oggetto sta andando in avanti
        if(vx > 0) {
            this.position.setLocation(maxX - widthP, this.position.getY());
        }else if(vx < 0){
            this.position.setLocation(0, this.position.getY());
        }
        return vx *= (-1 * (1- enrgAssPerc));
    }
    
    private double invertVY(double vy){
        //sel oggetto sta cadendo
        if(vy < 0) {
            this.position.setLocation(this.position.getX(), 0);
        }
        //se l oggetto sta salendo
        else if (vy > 0){
            this.position.setLocation(this.position.getX(), maxY - heightP);
        }
        return vy *= (-1 * (1 - enrgAssPerc));
    }
    
    private double slowVX(double vx, double vy, double time){
        //se il proiettile tocca il bordo alto o basso
        if(this.position.getY() == 0 || this.position.getY() - heightP == maxY){
            //se la velocita del poiettile e minore di quanto verra decrementata dall attrito allora si ferma
            if(Math.abs(vx) < atrTerreno * time){
                vx = 0;
            }
            
            //se va in aavanti
            if(vx > 0){
                return vx - atrTerreno * time;
            //se va indietro
            }else if (vx < 0){
                return vx + atrTerreno * time;
            //se e fermo
            }else{
                return 0;
            }
        }
        else{
            return vx;
        }
    }
	
    @Override
    public void run(){
        boolean flag = true;

        while(flag){
                try{
                    Thread.sleep(20); //pausa
                    newPos(); // calcolo la nuova posizione dopo la pausa
                    lp.hasMoved(this.position);
                    
                    //fermo il thread se il poiettile e fermo
                    if(intV == 0 && angV == 0){
                        this.t.interrupt();
                    }
                }catch(InterruptedException e){
                    flag = false;
                }	
        }
        //System.out.println("stopped");
    }
}