# Projectile Motion

This is a 2D implementation of the physical formula for projectile motion.

![img.png](img.png)

$$ x(t) = x_0 + \frac{1}{2} a_x t^2 + v_{0x} t $$

Also, during the shock with the ground, the projectile will lose some energy.
This is simulated with a coefficient of restitution.

Then, when not bouncing and only sliding on the ground, the projectile will lose energy due to friction.

## Requirements

- Java (6 to) 8 (max, recommended)
- NetBeans (highly recommended)

Java Applets have been dropped with Java 9. So you need an older versione to run this project.
Java 8 is the preferred version. Until Java 6 should be ok.

Problems running the project are expected, applets are deprecated and not supported by modern browsers.**

This is a NetBeans Project. Using NetBeans isn't mandatory but recommended for the built-in applet runner.
You can run the GrigliaApplet.java class with netbeans.

[NetBeans Instructions](https://netbeans.apache.org/tutorial/main/kb/docs/java/javase-jdk8/)

Alternatively, you can use IntellijJ IDEA with the [Applet runner plugin](https://plugins.jetbrains.com/plugin/16682-applet-runner) (not tested).

## Installation and run

Clone the repo:

```bash
git clone git@gitlab.com:stefanorossiti/projectiles.git
```

Run `AppletProiettile.java` with your preferred IDE (NetBeans is recommended) 
using 
java 8.

## License

All my work is released under [DBAD](https://www.dbad-license.org/) license.